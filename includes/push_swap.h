
#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H
# include "libft.h"
# include <unistd.h>	// read(), write(), sleep()
# include <stdlib.h>	// malloc(), free(), exit(), atoi()

# define INT_MAX 2147483647
# define INT_MIN -2147483648

typedef struct		s_stack
{
	int				n;
	int				limit;
	struct s_stack	*next;
}					t_stack;

/*
** algo_bubble_sort.c // sort_luke.c
*/
int			luke_bubble_sort(t_stack **a, t_stack **b, t_list *solution);

/*
** pushswap.c
*/
int			check_flag(int *ac, char ***av);
void		is_valid(int ac, char **av);
t_stack		*init_stack(int ac, char **av);
t_list		*launch_algo(t_stack **a, t_stack **b, int i);

/*
**	stop.c
*/
void		print_test(t_list *lst, char *s);
void		ps_usage(void);
void		ps_error(int i);
void		ps_stop(char **tab, t_list *lst, int i);

/*
** algo.c // sort.c
*/
int			nbr_element_smaller(t_stack *list, int size, int pivot);
int			find_pivot(t_stack *list, int size);
int			divide_a(t_stack **a, t_stack **b, t_list *solution);
int			divide_b(t_stack **a, t_stack **b, t_list *solution);
void		send_sublist_to_a(t_stack **a, t_stack **b, t_list *solution);
void		hugo_sort(t_stack **a, t_stack **b, t_list *solution);

/*
** sort_recursif.c
*/

/*
** sort_utils.c
*/
int			find_biggest(t_stack *list, int size);
int			find_smallest(t_stack *list, int size);
int			sublist_size(t_stack *list);

/*
** spcecial_sorts_3_5.c // sort_specials_3_5.c
*/
void		special_sort_3(t_stack **a, t_list *solution);
void		special_sort_5(t_stack **a, t_stack **b, t_list *solution);

/*
** minisort.c
*/
void		minisort(t_stack **list, t_list *solution);

/*
** minisort_by_rank.c // minisort_rank.c
*/
int			find_rank(t_stack *list, int nbr, int i);
int			order_is(t_stack *list, int nbr);
void		sort_5(t_stack **a, t_list *solution);
void		sort_4(t_stack **a, t_list *solution);
void		sort_3(t_stack **a, t_list *solution);
void		sort_2(t_stack **a, t_list *solution);
void		mini_sort(t_stack **a, t_list *solution);

/*
** minisort_2ways_bubble_sort.c // minisort_bubble.c
*/
void		mark_sublist(t_stack *list);
int			find_smallest_bubble(t_stack *list);
void		bubble_sort(t_stack **a, t_stack **b, t_list *solution);

/*
** print.c
*/
void		mark_step(t_list *solution, char *step);
void		fill_solution(t_list *lst, char *s);
char		*fill_line(t_stack *list, char *stack);
void		print_result(t_list *lst, int i);

/*
** swap
*/
t_list		*sa(t_stack **a, t_list **solution);
t_list		*sb(t_stack **b, t_list **solution);
t_list		*ss(t_stack **a, t_stack **b, t_list **solution);

/*
** push
*/
t_list		*pa(t_stack **a, t_stack **b, t_list **solution);
t_list		*pb(t_stack **b, t_stack **a, t_list **solution);

/*
** rotate
*/
t_list		*ra(t_stack **a, t_list **solution);
t_list		*rb(t_stack **b, t_list **solution);
t_list		*rr(t_stack **a, t_stack **b, t_list **solution);

/*
** reverse rotate
*/
t_list		*rra(t_stack **a, t_list **solution);
t_list		*rrb(t_stack **b, t_list **solution);
t_list		*rrr(t_stack **a, t_stack **b, t_list **solution);

#endif
