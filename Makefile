
# - - - - - - - - - - - - - - # name =	value \			 | .c in srcs/
#       variables names       # 		value			 | .h in includes/
# - - - - - - - - - - - - - - # ! name is case sensitive | ! use VPATH only for .c

NAME	=	push_swap

CC		=	gcc

VPATH	=	srcs

IDIR	=	./includes

_DEPS	=
DEPS	=	$(_DEPS:%.h=$(IDIR)/%.h)
# used to check if a .h has been modify when execute $NAME rule

LDIR	=	./libft
_LIBS	=	libft.a
LIBS	=	$(_LIBS:lib%.a=%)

SRCS	=	push_swap.c \
			algo.c \
			print.c \
			stop.c \
			swap.c \
			push.c \
			rotate.c \
			reverse_rotate.c \
			algo_bubble_sort.c \
			minisort.c \
			minisort_by_rank.c \
			minisort_2ways_bubble_sort.c \
			special_sorts_3_5.c \
			sort_utils.c


ODIR	=	./builds
OBJS	=	$(SRCS:%.c=$(ODIR)/%.o)

CFLAGS	=	-I$(IDIR) -I./libft/includes/ -g3 -Wall -Wextra -Werror

LFLAGS	=	-L$(LDIR) -l$(LIBS)



# - - - - - - - - - - - - - - #	target:	prerequisites	| $@ : target
#      rules to execute       #			recipe			| $< : 1st prerequisite
# - - - - - - - - - - - - - - #			recipe			| $^ : all prerequisites

all: $(NAME)

$(NAME): $(ODIR) $(OBJS) $(DEPS)
	make -C $(LDIR)
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(LFLAGS)

$(ODIR):
	mkdir -p $@

$(ODIR)/%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

libft:
	make re -C $(LDIR)
	make $(NAME)

clean:
	/bin/rm -f $(OBJS)

fclean: clean
	/bin/rm -rf $(ODIR)
	/bin/rm -f $(NAME)
	/bin/rm -rf a.out a.out.dSYM

re: fclean all

leaks: CFLAGS += -fsanitize=address
leaks: clean $(NAME)

valgrind: clean $(NAME)
	valgrind --leak-check=full --show-leak-kinds=all ./$(NAME) -p 4 67 9 76 98 7 1

testp : clean $(NAME)
	./$(NAME) -p 76 81 10 11 12 15 14 27 34 28 17 18 19 20 36 21 23 37 22 24 25 26 29 30 31 72 33 53 46 32 35 91 38 39 40 45 78 41 57 71 54 43 42 44 66 50 47 48 49 79 51 56 93 65 64 63 67 90 58 59 60 62 61 87 68 74 82 85 86 77 75 70 73 89 69 83 84 92 88 95 94 96 99 97 98 100 1 2 3 4 5 6 7 8 9 52 16 55 80 13

test : clean $(NAME)
	./$(NAME) 76 81 10 11 12 15 14 27 34 28 17 18 19 20 36 21 23 37 22 24 25 26 29 30 31 72 33 53 46 32 35 91 38 39 40 45 78 41 57 71 54 43 42 44 66 50 47 48 49 79 51 56 93 65 64 63 67 90 58 59 60 62 61 87 68 74 82 85 86 77 75 70 73 89 69 83 84 92 88 95 94 96 99 97 98 100 1 2 3 4 5 6 7 8 9 52 16 55 80 13

# 83 94 28 17 18 36 20 33 39 53 23 37 22 24 45 25 29 78 26 30 31 72 46 32 35 70 38 93 50 91 40 5 41 57 71 66 92 54 67 75 65 42 43 44 74 79 47 48 49 88 51 63 7 68 87 61 82 4 90 58 59 62 60 1 85 89 96 98 100 84 69 77 73 3 86 99 97 6 2 9 8 52 80 16 55 13 76 81 10 11 12 15 14 27 34 56 21 64 95 19

.PHONY: all clean fclean re gcc

