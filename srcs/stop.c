#include "push_swap.h"

/*
** if err = 0, the list are freed but the program doesn't stop
** if err = 1, 2 or 3, a message is printed and the program stop
** if err = 3 ou -1, the parameter list is just a t_stack *list
*/
void	ps_stop(char **tab, t_list *lst, int err)
{
	t_stack	**a;
	t_stack	**b;

	if (tab != NULL)
		ft_free_tab(tab);
	if (err == 3 || err == -1)
		ft_lstclear(&lst, NULL);
	else if (lst)
	{
		a = lst->content;
		b = lst->next->content;
		ft_lstclear((t_list **)a, NULL);
		ft_lstclear((t_list **)b, NULL);
		lst->content = NULL;
		lst->next->content = NULL;
		ft_lstclear(&lst, free);
	}
	if (err > 0)
	{
		if (err == 1)
			ft_printf("usage :\nlaunch executable : ./push_swap [flag -p] nb nb nb nb nb ...\n");
		if (err >= 2)
			ft_putstr_fd("Error\n", 2);
		exit(0);
	}
}
