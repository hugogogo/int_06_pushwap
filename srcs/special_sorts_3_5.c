#include "push_swap.h"

void	special_sort_3(t_stack **a, t_list *solution)
{
	int	order;
	int	size;

	size = sublist_size(*a);
	order = order_is(*a, size);
	if (order == 12)
		sa(a, &solution);
	if (order == 123)
	{
		ra(a, &solution);
		sa(a, &solution);
	}
	if (order == 132)
		rra(a, &solution);
	if (order == 213)
		ra(a, &solution);
	if (order == 231)
	{
		rra(a, &solution);
		sa(a, &solution);
	}
	if (order == 312)
		sa(a, &solution);
}

void	special_sort_5(t_stack **a, t_stack **b, t_list *solution)
{
	int	size;
	int	position;
	int	i;

	size = sublist_size(*a);
	i = size - 3;
	while (size > 3)
	{
		position = find_smallest(*a, size);
		if (size - position >= position - 1)
		{
			while(--position)
				ra(a, &solution);
		}
		else
			while(position++ <= size)
				rra(a, &solution);
		pb(b, a, &solution);
		size--;
	}
	special_sort_3(a, solution);
	while (i--)
			pa(a, b, &solution);
}

