#include "push_swap.h"

int	find_rank(t_stack *list, int nbr, int i)
{
	t_stack	*head;
	int		comp;
	int		rank;
	int		j;

	head = list;
	j = 0;
	while (++j <= nbr - i)
		head = head->next;
	comp = head->n;
	head = list;
	j = 0;
	rank = 1;
	while (++j <= nbr)
	{
		if (comp > head->n)
			rank++;
		head = head->next;
	}
	return (rank);
}

int	order_is(t_stack *list, int nbr)
{
	int		order;
	int		rank;
	int		i;

	i = 0;
	order = 0;
	while (++i <= nbr)
	{
		rank = find_rank(list, nbr, i);
		order = order * 10 + rank;
	}
	return (order);
}

//       |12345    |....5      
// s     |12354    |...5.      
// r    4|1235    .|...5       
// s    4|1253    .|..5.       
// r   34|125    ..|..5        
// s   34|152    ..|.5.        
// r   34|15    ...|.5         
// s   34|51    ...|5.         
//                             
// rr  34|512    ..|5..        
// rr   4|5123    .|5...       
// rr    |51234    |5....      
//                             
// s     |51243    |.1243      
// r    3|5124    3|.124       
// s    3|5142    3|.142       
// r   23|514    23|.14        
// s   23|541    23|.41        
// rr   3|5412    3|.412       
// rr    |54123    |.4123      
// s     |54132    |.4132      
// r    2|5413    2|.413       
// s    2|5431    2|.431       
// rr    |54312    |.4312      
// s     |54321    |.4321      
//                          
//                          
//                          
// s     |1243    |..4.     
// r    3|124    .|..4      
// s    3|142    .|.4.      
// r   23|14    ..|.4       
// s   23|41    ..|4.       
// rr   3|412    .|4..      
// rr    |4123    |4...     
//                          
// s     |4132    |.132     
// r    2|413    2|.13      
// s    2|431    2|.31      
// rr    |4312    |.312     
// s     |4321    |.321     
//
void	sort_5(t_stack **a, t_list *solution)
{
	int	order;

// algos de 5 uniquement necessaires pour trier cette liste :
// 5 10 2 7 65 -12 -3 6 12 28 17 13 54 83 20 11 34 21 67 48
// 48 67 83 54 65 | 21 34 20 17 28 | 11 13 12 7 10 | 6 -3 -12 2 5
// 1  4  5  2  3  | 3  5  2  1  4  | 3  4  5  1  2 | 5  2  1  3 4

	order = order_is(*a, 5);
	if (order == 12345)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	/*
	else if (order == 12354)
	else if (order == 12435)
	else if (order == 12453)
	else if (order == 12534)
	else if (order == 12543)

	else if (order == 13245)
	else if (order == 13254)
	else if (order == 13425)
	else if (order == 13452)
	else if (order == 13524)
	else if (order == 13542)

	else if (order == 14235)
	*/
	else if (order == 14253)
	{
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	/*
	else if (order == 14325)
	else if (order == 14352)
	else if (order == 14523)
	else if (order == 14532)

	else if (order == 15234)
	else if (order == 15243)
	else if (order == 15324)
	else if (order == 15342)
	else if (order == 15423)
	else if (order == 15432)


	else if (order == 21345)
	else if (order == 21354)
	else if (order == 21435)
	else if (order == 21453)
	else if (order == 21534)
	else if (order == 21543)

	else if (order == 23145)
	else if (order == 23154)
	else if (order == 23415)
	else if (order == 23451)
	else if (order == 23514)
	else if (order == 23541)

	else if (order == 24135)
	else if (order == 24153)
	else if (order == 24315)
	else if (order == 24351)
	else if (order == 24513)
	else if (order == 24531)

	else if (order == 25134)
	else if (order == 25143)
	else if (order == 25314)
	else if (order == 25341)
	else if (order == 25413)
	else if (order == 25431)


	else if (order == 31245)
	else if (order == 31254)
	else if (order == 31425)
	else if (order == 31452)
	else if (order == 31524)
	else if (order == 31542)

	else if (order == 32145)
	else if (order == 32154)
	else if (order == 32415)
	else if (order == 32451)
	else if (order == 32514)
	else if (order == 32541)

	else if (order == 34125)
	else if (order == 34152)
	else if (order == 34215)
	else if (order == 34251)
	*/
	else if (order == 34512)
	{
		ra(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	/*
	else if (order == 34521)

	else if (order == 35124)
	else if (order == 35142)
	*/
	else if (order == 35214)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
	}
	/*
	else if (order == 35241)
	else if (order == 35412)
	else if (order == 35421)


	else if (order == 41235)
	else if (order == 41253)
	else if (order == 41325)
	else if (order == 41352)
	else if (order == 41523)
	else if (order == 41532)

	else if (order == 42135)
	else if (order == 42153)
	else if (order == 42315)
	else if (order == 42351)
	else if (order == 42513)
	else if (order == 42531)

	else if (order == 43125)
	else if (order == 43152)
	else if (order == 43215)
	else if (order == 43251)
	else if (order == 43512)
	else if (order == 43521)

	else if (order == 45123)
	else if (order == 45132)
	else if (order == 45213)
	else if (order == 45231)
	else if (order == 45312)
	else if (order == 45321)


	else if (order == 51234)
	else if (order == 51243)
	else if (order == 51324)
	else if (order == 51342)
	else if (order == 51423)
	else if (order == 51432)
	*/

	else if (order == 52134)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	/*
	else if (order == 52143)
	else if (order == 52314)
	else if (order == 52341)
	else if (order == 52413)
	else if (order == 52431)

	else if (order == 53124)
	else if (order == 53142)
	else if (order == 53214)
	else if (order == 53241)
	else if (order == 53412)
	else if (order == 53421)

	else if (order == 54123)
	else if (order == 54132)
	else if (order == 54213)
	else if (order == 54231)
	else if (order == 54312)
	else if (order == 54321)
	*/
}

void	sort_4(t_stack **a, t_list *solution)
{
	int	order;

	order = order_is(*a, 4);
	if (order == 1234)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 1243)
	{
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 1324)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 1342)
	{
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 1423)
	{
		sa(a, &solution);
		ra(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 1432)
	{
		ra(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 2134)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	else if (order == 2143)
	{
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	else if (order == 2314)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	else if (order == 2341)
	{
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	else if (order == 2413)
	{
		sa(a, &solution);
		ra(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	else if (order == 2431)
	{
		ra(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	else if (order == 3124)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 3142)
	{
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 3214)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
	}
	else if (order == 3241)
	{
		ra(a, &solution);
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
	}
	else if (order == 3412)
	{
		sa(a, &solution);
		ra(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
	}
	else if (order == 3421)
	{
		ra(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		rra(a, &solution);
	}
	else if (order == 4123)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 4132)
	{
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 4213)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	else if (order == 4231)
	{
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	else if (order == 4312)
		sa(a, &solution);
}

void	sort_3(t_stack **a, t_list *solution)
{
	int	order;

	order = order_is(*a, 3);
	if (order == 123)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 132)
	{
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
		sa(a, &solution);
	}
	else if (order == 213)
	{
		sa(a, &solution);
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	else if (order == 231)
	{
		ra(a, &solution);
		sa(a, &solution);
		rra(a, &solution);
	}
	else if (order == 312)
		sa(a, &solution);
}

void	sort_2(t_stack **a, t_list *solution)
{
	int	order;

	order = order_is(*a, 2);
	if (order == 12)
		sa(a, &solution);
}

void	mini_sort(t_stack **a, t_list *solution)
{
	int	list_size;

	list_size = sublist_size(*a);
	if(list_size == 1)
		return ;
	(*a)->limit = 0;
	if(list_size == 2)
		sort_2(a, solution);
	else if(list_size == 3)
		sort_3(a, solution);
	else if(list_size == 4)
		sort_4(a, solution);
	else if(list_size == 5)
		sort_5(a, solution);
	/*
	else if(list_size == 6)
		sort_6(a, solution);
	else
		sort_big(a, b, solution);
	*/
	(*a)->limit = 1;
	mark_step(solution, "mini_sort");
}
