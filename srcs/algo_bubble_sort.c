/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo_bubble_sort.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperrey <lperrey@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/10 21:52:43 by lperrey           #+#    #+#             */
/*   Updated: 2021/06/10 22:49:04 by lperrey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	search_smaller(t_stack *stack, int *smaller)
{
	int		i;
	int		i_small;

	i_small = 0;
	i = 0;
	*smaller = (stack)->n;
	while (stack->next)
	{
		stack = stack->next;
		i++;
		if (stack->n < *smaller)
		{
			*smaller = stack->n;
			i_small = i;
		}
	}
	return (i_small);
}

int			luke_bubble_sort(t_stack **a, t_stack **b, t_list *solution)
{
	int	smaller;

	while (*a)
	{
		// ft_lstsize() pour verification du sens de rotation
		if (search_smaller(*a, &smaller) <= ft_lstsize((t_list*)*a) / 2)
		{
			while ((*a)->n != smaller)
				ra(a, &solution);
		}
		else
		{
			while ((*a)->n != smaller)
				rra(a, &solution);
		}
		pb(b, a, &solution);
	}
	while (*b)
		pa(a, b, &solution);
	return (1);
}
