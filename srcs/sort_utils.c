#include "push_swap.h"

// size is initialized to 1 because the loop check the next element, so it will end one step before reaching it
// it checks the next one to avoid handle the first one with ->limit set to 1
int	sublist_size(t_stack *list)
{
	int	size;

	if (list == NULL)
		return (0);
	size = 1;
	while (list->next != NULL && list->next->limit != 1)
	{
		list = list->next;
		size++;
	}
	return (size);
}

int	find_smallest(t_stack *list, int size)
{
	int	smallest;
	int	position;
	int	i;

	smallest = list->n;
	position = 1;
	i = 1;
	while (list && --size)
	{
		i++;
		list = list->next;
		if (smallest > list->n)
		{
			position = i;
			smallest = list->n;
		}
	}
	return (position);
}

int	find_biggest(t_stack *list, int size)
{
	int	biggest;
	int	position;
	int	i;

	biggest = list->n;
	position = 1;
	i = 1;
	while (list && --size)
	{
		i++;
		list = list->next;
		if (biggest < list->n)
		{
			position = i;
			biggest = list->n;
		}
	}
	return (position);
}

