#include "push_swap.h"

void	rotate(t_stack **stack)
{
	t_stack		*tmp;
	t_stack		*first;

	if (*stack && (*stack)->next)
	{
		first = *stack;
		tmp = *stack;
		*stack = (*stack)->next;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = first;
		first->next = NULL;
	}
}

t_list	*ra(t_stack **a, t_list **lst)
{
	rotate(a);
	fill_solution(*lst, "ra");
	return (NULL);
}

t_list	*rb(t_stack **b, t_list **lst)
{
	rotate(b);
	fill_solution(*lst, "rb");
	return (NULL);
}

t_list	*rr(t_stack **a, t_stack **b, t_list **lst)
{
	rotate(a);
	rotate(b);
	fill_solution(*lst, "rr");
	return (NULL);
}
