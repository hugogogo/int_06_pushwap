#include "push_swap.h"

void	push(t_stack **dst, t_stack **src)
{
	t_stack		*tmp1;
	t_stack		*tmp2;

	if (*src)
	{
		tmp1 = *src;
		tmp2 = *dst;
		*src = (*src)->next;
		tmp1->next = tmp2;
		*dst = tmp1;
	}
}

t_list	*pa(t_stack **a, t_stack **b, t_list **solution)
{
	push(a, b);
	fill_solution(*solution, "pa");
	return (NULL);
}

t_list	*pb(t_stack **b, t_stack **a, t_list **solution)
{
	push(b, a);
	fill_solution(*solution, "pb");
	return (NULL);
}
