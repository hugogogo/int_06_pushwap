#include "push_swap.h"

void	reverse_rotate(t_stack **stack)
{
	t_stack		*tmp;
	t_stack		*before;

	if (*stack && (*stack)->next)
	{
		tmp = *stack;
		while (tmp->next)
		{
			before = tmp;
			tmp = tmp->next;
		}
		tmp->next = *stack;
		*stack = tmp;
		before->next = NULL;
	}
}

t_list	*rra(t_stack **a, t_list **lst)
{
	reverse_rotate(a);
	fill_solution(*lst, "rra");
	return (NULL);
}

t_list	*rrb(t_stack **b, t_list **lst)
{
	reverse_rotate(b);
	fill_solution(*lst, "rrb");
	return (NULL);
}

t_list	*rrr(t_stack **a, t_stack **b, t_list **lst)
{
	reverse_rotate(a);
	reverse_rotate(b);
	fill_solution(*lst, "rrr");
	return (NULL);
}
