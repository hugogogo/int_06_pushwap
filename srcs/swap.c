#include "push_swap.h"

void	swap(t_stack **stack)
{
	t_stack		*tmp;

	if (*stack && (*stack)->next)
	{
		tmp = *stack;
		*stack = (*stack)->next;
		tmp->next = (*stack)->next;
		(*stack)->next = tmp;
	}
}

t_list	*sa(t_stack **a, t_list **solution)
{
	swap(a);
	fill_solution(*solution, "sa");
	return (NULL);
}

t_list	*sb(t_stack **b, t_list **solution)
{
	swap(b);
	fill_solution(*solution, "sb");
	return (NULL);
}

t_list	*ss(t_stack **a, t_stack **b, t_list **solution)
{
	swap(a);
	swap(b);
	fill_solution(*solution, "sb");
	return (NULL);
}
