#!/bin/bash

# colors variables
GREY='\033[1;30m'
RED='\033[1;31m'
GREEN='\033[1;32m'
ORANGE='\033[1;33m'
BLUE='\033[1;34m'
PURPLE='\033[1;35m'
LIGHTBLUE='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

# usage of arguments
if [ $# -eq 1 ]
then
	SIZE=$1		# size of the list
elif [ $# -eq 2 ]
then
	FLAG=$1		# flag for leaks and errors // not in use so far
	SIZE=$2
else
	echo "usage : ./tester.sh path/to/push_swap size_of_list"
	exit 0
fi

PRGM="push_swap"	# path to program
# range of random numbers
RANGE=$(($SIZE * 2))

# generate the list of randoms differents numbers
LIST=($(shuf -i 0-$RANGE -n $SIZE))
echo -e "${WHITE}test lancé avec la liste de nombres suivantes :${NC}"
echo -e "${LIST[@]}"

OUTPUT=$(./$PRGM ${LIST[@]})
COUNT=$(echo "$OUTPUT" | wc -l)
RESULT=$(echo "$OUTPUT" | ./checker ${LIST[@]})

if [[ $RESULT == "OK" ]]
then
	COLOR=$GREEN
	ANNONCE="réussi !"
else
	COLOR=$RED
	ANNONCE="raté :("
fi

echo -en "${WHITE}le test est ${NC}${COLOR}${ANNONCE}${NC}${WHITE} et a été "
echo -e "effectué en ${NC}${ORANGE}${COUNT}${NC}${WHITE} opérations${NC}"

