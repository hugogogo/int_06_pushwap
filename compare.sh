#!/bin/bash

# usage of arguments
if [ $# -eq 3 ]
then				# names = path to programs
	NAME1=$1
	NAME2=$2
	PRGRM1=$1		# path to programs
	PRGRM2=$2
	SIZE=$3			# size of the list
elif [ $# -eq 5 ]
then				# names given in arguments
	NAME1=$1
	NAME2=$3
	PRGRM1=$2		# path to programs
	PRGRM2=$4
	SIZE=$5			# size of the list
else
	echo "usage : ./compare_pushswap file1 file2 nb"
	echo "or    : ./compare_pushswap name1 file1 name2 file2 nb"
	exit 0
fi

# range of random numbers
RANGE=100
if [ $SIZE -gt 99 ]; then RANGE=1000; fi
if [ $SIZE -gt 999 ]; then echo "size too big"; exit 0; fi

# generate the list of randoms differents numbers
LIST=($(shuf -i 0-$RANGE -n $SIZE))
echo -e "${LIST[@]}"

# player equal number of line of programm output
PLAYER1=$(./$PRGRM1 ${LIST[@]} | wc -l)
PLAYER2=$(./$PRGRM2 ${LIST[@]} | wc -l)

# colors variables
RED='\033[1;31m'
GREEN='\033[1;32m'
NC='\033[0m'

if [ $PLAYER1 -lt $PLAYER2 ]
then
	COLOR1=$GREEN
	COLOR2=$RED
else
	COLOR1=$RED
	COLOR2=$GREEN
fi

echo -e "${COLOR1}$NAME1 : $PLAYER1${NC}"
echo -e "${COLOR2}$NAME2 : $PLAYER2${NC}"
